# Project Framework PHP (IMG Gallery)

This version is a development version.

"*Release version*" on [client branch](https://gitlab.com/alexis_forestier/projet-framework-php-cakephp/-/tree/client) !

## Requirement
To use this project, you must have [PHP 7.2+](https://www.php.net/downloads.php), 
[composer](https://getcomposer.org/download/) and all project [dependencies](#dependencies).

And [MySQL Server](https://dev.mysql.com/downloads/installer/) to host the database.

## Dependencies
*if you follow the [installation](#installation), you don't need to install them one by one*
In [composer.json](/projet-framework-php/composer.json)
- php : >=7.2
- cakephp/authentication : ^2.0
- cakephp/cakephp : ^4.3
- cakephp/migrations : ^3.2
- cakephp/plugin-installer : ^1.3
- mobiledetect/mobiledetectlib : ^2.8

## Installation

1. Start by cloning the project
- You can use the "clone" button above, and a method for Visual Studio Code.

Or
- Create a folder, in this one, enter `cmd` in the path bar

![](/README_img/img1.png)
- In your terminal : `git clone https://gitlab.com/alexis_forestier/projet-framework-php-cakephp.git`

2. In the **bin** folder's project, Use the composer install :
- For development : `composer install`
- For production : `composer install --no-dev`

accept permissions, with "y".

3. Check if you are **app_local.php** file in the **config** folder

(*if you don't have this file do step 2 again*)

For use the **Debug-kit**, config the line 18 as true :

![](/README_img/img2.png)

With SQL create a new database and go to line 47,48 and 50 to config this one.

![](/README_img/img3.png)

Save the file.

4. Migrate the database
- go to the **bin** folder with your terminal
- enter the command : `cake migration migrate`

5. Now, start the server !
- go into the **bin** folder with the terminal
- and enter this command : `cake server`
- if you want you can config the port, like this : `cake server -p [port_number]`

Finish ! bravo &#x1F44F;

