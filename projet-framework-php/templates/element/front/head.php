<head>
    <title><?= $name; ?></title>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php
        $this->Html->meta('icon', 'favicon.ico');
        
        if(!empty($css)){
            echo $this->Html->css($css);
        } 
    ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>

</head>