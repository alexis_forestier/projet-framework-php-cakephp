<?php
function getStyleLeft($authentication)
{
    if (!$authentication) {
        return 'style="left: -60px;"';
    }
    return null;
}
?>

<header>
    <div>
        <div>
            <?= $this->Html->image('SCPlogo.png', [
                'id' => 'logo',
                'alt' => 'Logo'
            ]) ?>

            <h1>
                <?= $this->Html->link('IMG Gallery', '/home') ?>
            </h1>
        </div>
        <div>
            <div id="language-selector">
                <p>
                    &#129147; <?= $this->request->getCookie('Language') ?? 'en_US' ?>
                </p>
                <ul>
                    <li>
                        <?= $this->Html->link(__('fr_FR'), '/language/fr_FR/') ?>
                    </li>
                    <li>
                        <?= $this->Html->link(__('en_US'), '/language/en_US/') ?>
                    </li>
                </ul>
            </div>
            <div id="header-user">
                <?= $this->Html->image('user-logo.svg', [
                    'alt' => 'User-logo'
                ]) ?>
                <ul <?= getStyleLeft($authentication) ?>>

                    <?php if (!$authentication) : ?>
                        <li><?= $this->Html->link(__('LOG IN'), '/login') ?></li>
                        <li><?= $this->Html->link(__('SIGN UP'), '/sign_in') ?></li>
                    <?php else : ?>
                        <?php
                        $id = $this->Session->read('User.id');
                        ?>
                        <li><?= $this->Html->link(__('PROFILE'), '/profile/' . $id . '/') ?></li>
                    <?php endif; ?>

                </ul>
            </div>

            <?php if ($authentication) : ?>
                <div>
                    <?= $this->Html->image('log-out.svg', [
                        'alt' => __('log out'),
                        'url' => ['controller' => 'users', 'action' => 'logout'],
                        'title' => __('log out')
                    ]) ?>
                </div>
            <?php endif; ?>

        </div>
    </div>
    <nav>
        <input type="checkbox" id="boutonmenu" name="menubutton">
        <span id="boutonmenu_span"></span>
        <ul>
            <li><?= $this->Html->link(__('HOME'), '/home') ?></li>
            <?php if ($authentication) : ?>
                <li><?= $this->Html->link(__('ADD IMAGE'), '/add') ?></li>
                <?php if ($this->Session->read('User.admin') === 1) : ?>
                    <li><?= $this->Html->link(__('MANAGE USERS'), '/manage') ?></li>

                <?php endif; ?>
            <?php endif; ?>
            <li><?= $this->Html->link(__('JSON'), '/api') ?></li>
        </ul>
    </nav>
</header>