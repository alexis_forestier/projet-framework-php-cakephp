<div class="users_form_content">
    <h2><?= __('SIGN UP') ?></h2>
    <p><?= __('Sign up for an account by filling in the information below.') ?></p>
    <?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Sign up on our website') ?></legend>
        <?= $this->Form->control('email',['label' => __('Email')]) ?>
        <?= $this->Form->control('firstname',['label' => __('First Name')]) ?>
        <?= $this->Form->control('lastname',['label' => __('Last Name')]) ?>
        <?= $this->Form->control('password',['label' => __('Password')]) ?>

        <?= $this->Form->button(__('Sign up')); ?>
    </fieldset>
    <?= $this->Form->end() ?>
</div>