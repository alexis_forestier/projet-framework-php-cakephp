<div class="users_form_content">
    <h2><?= __('LOGIN') ?></h2>
    <p><?= __('You need an account to access some parts of our website.') ?></p>
    <p>
        <?= $this->Html->link(__('You can register for an account here.'), '/sign_in/') ?>
    </p>

    <?= $this->Form->create() ?>

    <fieldset>
        <legend><?= __('Please enter your email and password') ?></legend>
        <?= $this->Form->control('email',['label' => __('Email')]) ?>
        <?= $this->Form->control('password',['label' => __('Password')]) ?>

        <?= $this->Form->button(__('Login')); ?>
    </fieldset>
    
    <?= $this->Form->end() ?>

</div>