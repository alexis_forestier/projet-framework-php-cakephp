<ul>

<?php
foreach ($users as $user) {
    echo "<li>";
    echo $user->firstname. ' ' .$user->lastname;
    echo $this->Form->postLink(
        __('Delete'),
        [
            'action' => 'delete',
            $user->id,
        ],
        [
            'block' => false, // disable inline form creation
            'confirm' => __('Are you sure you want to delete this user ?')
        ]
    );
    echo "</li>";
}
?>

</ul>