<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

    //CSS from controller
    $css = $css ?? 'styles';

    //Script from controller
    $script = $script ?? '';

    //Title from controller
    $pageTitle = $pageTitle ?? __('IMG gallery');

?>

<!DOCTYPE html>
<html lang="en">

<!--HEAD-->
<?= $this->element('front/head', array(
    'name' => $pageTitle, 
    'css' => $css
)) ?>

<body>
    <!-- HEADER -->
    <?=  $this->element('front/header') ?>

    <main class="main">
        <div class="side"></div>
        <div  id="content">

            <!--Flash-->
            <?= $this->Flash->render(); ?>

            <?= $this->fetch('content') ?>
        </div>
        <div class="side"></div>
    </main>

    <!-- FOOTER -->
    <?=  $this->element('front/footer') ?>

    <?= $this->Html->script(['script']) ?>
    <?= $this->fetch('script') ?>
</body>


</html>
