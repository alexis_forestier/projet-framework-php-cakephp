<?php


$ext = explode("/", $_data->path);
$ext = array_pop($ext);
$ext = explode(".", $_data->path);
$ext = '.' . array_pop($ext);

function getDeleteLink($session, $html, $_data, $value){
    if($session->read('User.id') == $_data->user_id || $session->read('User.admin') == 1){
        return $html->Link(__('Delete'),
        ['controller' => 'Comments', 'action' => 'delete', $value->id],
        ['class' => 'comments-supp', 'confirm' => __('Are you sure you wish to delete this comment ?')]
    );
    }
}

if (!empty($_data)) :

?>

    
    <section id="show-content">
        <h2><?= $_data->name ?></h2>

        <?= $this->Html->image('/' . $_data->path, ['alt' => $_data->description]) ?>
        <table>
            <tr>
                <th><?= __('Author')?></th>
                <td><?= $_data->author ?></td>
            </tr>
            <tr>
                <th><?= __('Description')?></th>
                <td><?= $_data->description ?></td>
            </tr>
            <tr>
                <th><?= __('Width')?></th>
                <td><?= $_data->width ?>px</td>
            </tr>
            <tr>
                <th><?= __('Height')?></th>
                <td><?= $_data->height ?>px</td>
            </tr>
            <tr>
                <th><?= __('Extension')?></th>
                <td><?= $ext ?></td>
            </tr>
            <tr>
                <th><?= __('Published')?></th>
                <td><?= $_data->created ?></td>
            </tr>
        </table>

        <?= $this->Html->link(
            __('Download this image !'),
            $_data->path,
            ['download' => strtolower(str_replace(' ', '_', $_data->name) . $ext), 'class' => 'btn-download']
        ); ?>

        <?php
        $id = $this->Session->read('User.id');

        if($authentication && $_data->user_id == $id){
            echo $this->Html->link(
                __('Edit Description'),
                '/edit/' . $_data->id . '/'
            );
        }
        
        if ($authentication && ($_data->user_id == $id || $this->Session->read('User.admin') == 1)) {

            echo $this->Html->link(
                __('Delete'),
                '/delete/' . $_data->id . '/',
                ['confirm' => __('Are you sure you wish to delete this image ?')]
            );
        }

        ?>
    </section>



<?php endif; ?>

<section class="comments-section">
    <h2><?= __('Comments') ?></h2>
    <?php
    foreach ($_data->comments as $value) {
        echo '<div class="comments">
                    <p class="comments-author">' . $value->author . '</p>
                    <p class="comments-date">' . $value->modified . '</p>
                    <p class="comments-content">' . $value->content . '</p>'
                    .getDeleteLink($this->Session,$this->Html, $_data, $value).
                '</div>';
    }
    ?>
</section>

<?php

    $session = $this->Session->read('User');
    if($session){
        $author = $session['firstname'] . ' ' . $session['lastname'];
    }
    else{
        $author = "";
    }
    

    echo $this->Form->create(null, [
        'type' => 'post',
        'url' => [
            'controller' => 'Comments',
            'action' => 'add'
        ],
        'id' => 'comments-form-add'
    ]);

    echo $this->Form->control('author', ['type' => 'text', 'value' => $author]);

    echo $this->Form->control('content', ['type' => 'textarea', 'id' => 'contentTextarea']);

    echo $this->Form->button(__('Add'), ['class' => 'btn']);

    //End form
    echo $this->Form->end();


?>