<?php

echo $this->Form->create(null, [
    'type' => 'post',
    'url' => [
        'action' => 'edit',
        $image->id
    ]
]);

//Description
echo $this->Form->control('description', ['type' => 'textarea', 'label' => __('Description'), 'value' => $image->description]);

//btn-submit
echo $this->Form->button(__('Add'));

//End form
echo $this->Form->end();