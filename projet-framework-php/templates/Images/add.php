<div class="images_form_content">
    <h2><?= __('ADD IMAGE') ?></h2>
    <p>Choose an image from your computer and upload it here!</p>
    <?php

    $session = $this->Session->read('User');
    $author = $session['firstname'] . ' ' . $session['lastname'];

    //Create form
    echo $this->Form->create(null, [
        'type' => 'file',
        'url' => [
            'action' => 'add'
        ],
    ]);

    //Image
    echo $this->Form->control('image', ['type' => 'file', 'label' => __('Your image')]);
    //Title
    echo $this->Form->control('title', ['type' => 'text', 'label' => __('Title')]);
    //Description
    echo $this->Form->control('description', ['type' => 'textarea', 'label' => __('Description')]);
    //Author
    echo $this->Form->control('author', ['type' => 'text', 'label' => __('Author'), 'value' => $author]);
    //btn-submit
    echo $this->Form->button(__('Add'));


    //End form
    echo $this->Form->end();

    ?>
</div>