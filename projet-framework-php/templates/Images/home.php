<?php

function getPagination($Page)
{
    $string = '<div class="divButton">';
    if ($Page > 1) :
        $string .= '<a href="/home/' . $Page - 1 . '">&laquo; ' . __('Previous') . '</a>';
    endif;
    $string .= '<a href="/home/' . $Page + 1 . '">' . __('Next') . ' &raquo;</a>
                    </div>';
    return $string;
}

?>



<?php
$here = $this->request->getParam("_matchedRoute");
if ($here == '/home' || $here == '/home/') : ?>
    <section>
        <h2><?= __('Welcome') ?></h2>
        <p id="intro"><?= __('So try typing the name of the image you are looking for, maybe someone has already published it !') ?></p>
    </section>
<?php endif; ?>

<!--SearchBar-->
<div class="divSearch">
    <?php
    echo $this->Form->create(null, [
        'type' => 'get',
        'url' => [
            'action' => 'home'
        ],
    ]);

    //Search
    echo $this->Form->control('search', ['type' => 'search', 'label' => false, 'value' => $search]);
    //btn-submit
    echo $this->Form->button(__('Search'));


    //End form
    echo $this->Form->end();
    ?>
</div>

<!--Pagination-->
<?= getPagination($Page) ?>


<!--Content-->
<section id="home-content">
    <?php
    

    foreach ($_data as $value) {

        $ext = explode("/", $value->path);
        $ext = array_pop($ext);
        $ext = explode(".", $value->path);
        $ext = '.' . array_pop($ext);

        if ($value != '') {
            echo '<div>';
            echo '<h3>'.$value->name.'</h3>';
            echo $this->Html->image('/' . $value->path, [
                'alt' => $value->description,
                'title' => $value->name,
                'url' => '/show/' . $value->id
            ]);
            echo '<p>'.$value->author.'</p>';
            echo '</div>';
        }
    }

    ?>
</section>


<!--Pagination-->
<?= getPagination($Page) ?>