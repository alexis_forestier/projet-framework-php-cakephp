//
// Ce fichier js comporte uniquement les fonctions et variables utilisées sur plusieurs pages
//

// CONST
let currentUrl = window.location.pathname
currentUrl = currentUrl.split("/");
currentUrl = currentUrl[currentUrl.length - 1];
/*let allPage = document.querySelectorAll('body > header > nav > ul a')     //Remplacer par le PHP*/

const ROOT = document.documentElement
const REGEX_EMAIL = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const REGEX_MDP = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
//
/**
 * Remet la couleur de selection des inputs a "Rouge"
 * "Rouge" étant la couleur par défault
 */
function reset_input_color() {
    ROOT.style.setProperty('--input_color', 'rgb(255, 0, 0)');
    ROOT.style.setProperty('--input_color_alpha', 'rgba(255, 0, 0, 0.5)');
};

/**
 * Si l'input séléctionner (focus) est déjà valide
 * la couleur de séléction est verte
 */
function green_color() {
    if (this.style.border == "1px solid rgb(0, 177, 0)") {
        ROOT.style.setProperty('--input_color', 'rgb(0, 177, 0)');
        ROOT.style.setProperty('--input_color_alpha', 'rgba(0, 177, 0, 0.25)');
    }
}

// Initialisation des inputs contenus dans les pages
const inp = document.getElementsByTagName("input");
for (let i = 0; i < inp.length; i++) {
    inp[i].addEventListener('blur', reset_input_color);
    inp[i].addEventListener('focus', green_color);
}

/**
 * Permet de changer la couleur du bouton de la barre de navigation correspondant à la page actuelle
 * Désactiver
 * Remplacer par le PHP
 */
/*
function active_page() {
    currentUrl = currentUrl.split("/");
    currentUrl = currentUrl[currentUrl.length - 1];
    for(let i = 0; i < allPage.length; i++) {
        if(allPage[i].getAttribute('href').includes(currentUrl)) {
            allPage[i].style.backgroundColor = "var(--secondary_color)";
        }
    }
}

try{
    active_page()
}catch(error){
    console.error("Probleme de active_page");
}*/


/**
 * Vérifie si l'email est correctement écrit.
 * Si la valeur de l'input email est correct, la bordure devient verte. 
 * Sinon la bordure devient rouge.
 */

const EMAIL = document.getElementById('email');
const labelEmail = document.getElementById('label-email');

function emailfunction() {
    if (REGEX_EMAIL.test(EMAIL.value.toLowerCase()) && EMAIL.value != "") {
        EMAIL.style.border = "solid 1px #00b100";
        ROOT.style.setProperty('--input_color', 'rgb(0, 177, 0)');
        ROOT.style.setProperty('--input_color_alpha', 'rgba(0, 177, 0, 0.25)');
        if (currentUrl == "login.php") {
            labelEmail.style.color = "#00b100";
        }
    }
    else {
        EMAIL.style.border = "solid 1px red";
        ROOT.style.setProperty('--input_color', 'rgb(255, 0, 0)');
        ROOT.style.setProperty('--input_color_alpha', 'rgba(255, 0, 0, 0.25)');
        if (currentUrl == "login.php") {
            labelEmail.style.color = "red";
        }
    }

    validation()
};

try {
    document.getElementById('email').addEventListener('keyup', emailfunction)
} catch (error) {
    if (currentUrl == "inscription.php" || currentUrl == "contact.php") {
        //console.error("Erreur : aucun element email");
    }
}

/**
 * Vérifie si le mot de passe est correctement écrit.
 * Si la valeur de l'input password est correct, la bordure devient verte. 
 * Sinon la bordure devient rouge.
 */


const PASSWORD = document.getElementById('password');
const labelPW = document.getElementById('label-password');

function passwordfunction() {
    if (REGEX_MDP.test(PASSWORD.value) && PASSWORD.value != "") {
        PASSWORD.style.border = "solid 1px #00b100";
        ROOT.style.setProperty('--input_color', 'rgb(0, 177, 0)');
        ROOT.style.setProperty('--input_color_alpha', 'rgba(0, 177, 0, 0.25)');
        if (currentUrl == "login.php") {
            labelPW.style.color = "#00b100";
        }
    }
    else {
        PASSWORD.style.border = "solid 1px red";
        ROOT.style.setProperty('--input_color', 'rgb(255, 0, 0)');
        ROOT.style.setProperty('--input_color_alpha', 'rgba(255, 0, 0, 0.25)');
        if (currentUrl == "login.php") {
            labelPW.style.color = "red";
        }
    }
    validation()
}

try {
    PASSWORD.addEventListener('keyup', passwordfunction);
} catch (error) {
    //console.error("Erreur : aucun element password");
}


/**
 * Vérifie les inputs ne nécéssitant pas de regex
 * Si la valeur de l'input contient au moins 1 caractère, la bordure devient verte. 
 * Sinon la bordure devient rouge.
 * @param {input} input 
 */
function veriffunction(input) {
    if (input.value != "") {
        input.style.border = "solid 1px #00b100";
        ROOT.style.setProperty('--input_color', 'rgb(0, 177, 0)');
        ROOT.style.setProperty('--input_color_alpha', 'rgba(0, 177, 0, 0.25)');
    }
    else {
        input.style.border = "solid 1px red";
        ROOT.style.setProperty('--input_color', 'rgb(255, 0, 0)');
        ROOT.style.setProperty('--input_color_alpha', 'rgba(255, 0, 0, 0.25)');
    }
    validation()
};

try {    // INPUT présent dans Contact et Inscription
    const FNAME = document.getElementById('fname')
    const LNAME = document.getElementById('lname')
    FNAME.addEventListener('keyup', veriffunction.bind(null, FNAME))
    LNAME.addEventListener('keyup', veriffunction.bind(null, LNAME))
} catch (error) {
    if (currentUrl == "inscription.php" || currentUrl == "contact.php") {
        //console.error('Erreur : probleme EventListener sur prénom et nom')
    }
}

try {    // INPUT présent uniquement dans Inscription
    const ADDRESS = document.getElementById('address')
    const CITY = document.getElementById('city')
    ADDRESS.addEventListener('keyup', veriffunction.bind(null, ADDRESS))
    CITY.addEventListener('keyup', veriffunction.bind(null, CITY))
} catch (error) {
    if (currentUrl == "inscription.php") {
        //console.error('Erreur : probleme EventListener sur adresse, codepostal et ville')
    }
}


// En cliquant sur le logo du site, cela change le mode du site
const logo_button = document.getElementById("logo");
logo_button.addEventListener('click', toogleDarkMode);
if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
    ROOT.classList.add('dark');
}
else {
    ROOT.classList.add('light');
}

function toogleDarkMode() {
    if (ROOT.classList.contains('dark')) {
        ROOT.classList.remove('dark');
        ROOT.classList.add('light');
    }
    else {
        ROOT.classList.remove('light');
        ROOT.classList.add('dark');
    }
};


//Ouvrir et fermer le aside sur les pages Présentation et Chiffre 
let aside_state = false;

function toggleAside() {
    if (aside_state == false) {
        if (window.matchMedia("(max-width: 700px)").matches) {
            document.getElementById('Aside_Context').style.transform = 'translateX(75px)';
        }
        else {
            document.getElementById('Aside_Context').style.transform = 'translateX(50vw)';
        }

        aside_state = true;
    }
    else {
        document.getElementById('Aside_Context').style.transform = 'translateX(calc(100vw - 75px))';
        aside_state = false;
    }
}

try {
    const button_aside = document.querySelector('#Aside_Context span');
    button_aside.addEventListener('click', toggleAside);
} catch (error) {
    //console.error('Erreur : pas de partie aside contextuelle')
}