<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\NotFoundException;
use Cake\Http\Cookie\Cookie;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{


    private function getUsersByEmail($email)
    {
        $query = $this->Users->find()
            ->where(['email ' => $email])
            ->first();

        if ($query) {
            unset($query['password']);
            return $query;
        }
        return false;
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);

        $this->Authentication->allowUnauthenticated(['login', 'home', 'add', 'logout', 'changeLanguage']);
        $authentication = $this->getAuthentication();
        $this->set(compact('authentication'));
    }

    public function login()
    {
        $result = $this->Authentication->getResult();
        // If the user is logged in send them away.
        if ($result->isValid()) {
            $_data = $this->getRequest()->getData();

            $user = $this->getUsersByEmail($_data['email']);
            if ($user) {
                $session = $this->getRequest()->getSession();

                $session->write('User', $user);

                $target = '/home';
                return $this->redirect($target);
            } else {
                $this->Flash->error('This user doesn\'t exists');
            }
        }
        if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error('Invalid username or password');
        }

        $this->setView(
            'Log in | IMG Gallery',
            ['root', 'header-footer', 'default', 'login-signup']
        );
    }

    public function logout()
    {
        $this->Authentication->logout();
        return $this->redirect(['controller' => 'Users', 'action' => 'login']);
    }

    public function changeLanguage($language)
    {
        if ($language == 'fr_FR') {
            $this->response = $this->response->withCookie(new Cookie('Language', 'fr_FR'));
        } else {
            $this->response = $this->response->withCookie(new Cookie('Language', 'en_US'));
        }
        return $this->redirect($this->referer());
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {

            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->active = 1;

            // add admin if it's the first user
            $query = $this->Users->find();
            $query = $query->select(['count' => $query->func()->count('*')])->first();
            if ($query['count'] === 0) {
                $user->admin = 1;
            }

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));

        $this->setView(
            'Sign up | IMG Gallery',
            ['root', 'header-footer', 'default', 'login-signup']
        );
    }

    /**
     * Profile method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function profile($id)
    {
        $user = $this->Users->get($id);
        if (!$user) {
            throw new notFoundException(__('Error 404 : User not found'));
        } else {
            $this->set(compact('user'));
            $this->setView(
                $user->firstname . '\'s profile | IMG Gallery',
                ['root', 'header-footer', 'default', 'profile']
            );
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'images', 'action' => 'home']);
    }

    /**
     * manage method
     *
     * @return \Cake\Http\Response|null|void Redirects to index.
     */
    public function manage()
    {
        // Manage users page
        $session = $this->getRequest()->getSession();

        if ($session->read('User.admin') === 1) {
            $users = $this->Users->find()
            ->where(['admin'=> 0])
            ->all();
            $this->set(compact('users'));
            $this->setView(
                'admin manage panel | IMG Gallery',
                ['root', 'header-footer', 'default', 'manage']
            );
        }
        else{
            return $this->redirect(['controller' => 'images', 'action' => 'home']);
        }
    }
}
