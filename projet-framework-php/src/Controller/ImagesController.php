<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\NotFoundException;

define('NO_DATA', "no data");

define('_IMAGE_FOLDER', "img/jpg/");


/**
 * Images Controller
 *
 * @method \App\Model\Entity\Image[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ImagesController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();

        $this->Authentication->allowUnauthenticated(['json', 'home', 'show']);
        $authentication = $this->getAuthentication();
        $this->set(compact('authentication'));
        $_IMAGE_FOLDER = _IMAGE_FOLDER;
        $this->set(compact('_IMAGE_FOLDER'));
    }

    /**
     * formatImageDataToArray method
     *
     * @return array
     */
    private function formatImageDataToArray($_data): array
    {


        return array(
            "id" => $_data['id'],
            "file" => $_data['name'] ?? NO_DATA,
            "description" => $_data['description'] ?? NO_DATA,
            "path" => $_data['path'] ?? NO_DATA,
            "author" => $_data['author'],
            "width" => $_data['width'],
            "height" => $_data['height'],
            "html" => "<img src='/img/jpg/" . ($_data['description'] ?? '#') . "' alt='" . ($_data['description'] ?? NO_DATA) . "'>",
            "comments" => $_data['comments']
        );
    }


    /**
     * Json method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function json()
    {

        $query = $this->Images->find();

        //URL parameters
        $limit = $this->getRequest()->getQuery('limit');
        $name = $this->getRequest()->getQuery('name');
        $page = intval($this->getRequest()->getQuery('page'));

        $passedArgs = $this->request->getParam('pass');

        //Param
        if (!empty($passedArgs)) {
            if (preg_match("/[a-zA-Z]*/", $passedArgs[0])) {
                $query->where(['name' => $passedArgs[0]]);
            } else {
                $query->where(['id' => $passedArgs[0]]);
            }

            $query->first();
            if (empty($query)) {
                throw new BadRequestException(__('Error 400 : Bad Request'));
            }
        } else {
            //Query name
            if (!empty($name)) {
                $query->where('name LIKE "%' . $name . '%"');
            }

            //Query limit
            if (!empty($limit)) {
                $query->limit($limit);
            }

            //Query page
            if (!empty($page) && is_int($page)) {
                if (empty($limit)) {
                    $query->limit(10);
                }
                $query->page($page);
            }


            $query->contain(['Comments'])->all();
        }

        // format JSON
        $json_array = array();
        foreach ($query as $value) {
            array_push($json_array, $this->formatImageDataToArray($value));
        }
        $json = json_encode($json_array);
    
        return $this->response = $this->response->withType("application/json")->withStringBody($json);
    }


    /**
     * Home method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function home()
    {
        $query = $this->Images->find()
            ->order(['modified'=>'DESC'])
            ->limit(10);

        // Query search
        $search = $this->getRequest()->getQuery('search');
        if (!empty($search)) {

            $query->where('LOWER(name) LIKE "%' . strtolower($search) . '%"');
        }

        // Param page
        $Page = $this->request->getParam('pass')[0] ?? 1;
        if ($Page < 1) {
            $Page = 1;
        }

        $Page = intval($Page);


        $_data = $query->page($Page)->all();

        $this->set(compact('_data'));
        $this->set(compact('Page'));
        $this->set(compact('search'));

        $this->setView(
            'Home | IMG Gallery',
            ['root', 'header-footer', 'default', 'homeImage']
        );
    }

    /**
     * Show method
     *
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Http\Exception\NotFoundException When image not found.
     */
    public function show($imageId = null)
    {
        if ($imageId == '') {
            return $this->redirect('/home');
        }

        // ! JE MET CETTE LIGNE PARCE QUE J'AI PAS RÉUSSI À UTILISER CORRECTEMENT LA TABLE I18n
        $this->Images->Comments->setLocale('fr_FR');

        $_data = $this->Images->find()
            ->where(['id' => $imageId])
            ->contain(['Comments.I18n'])
            ->first();

        if (!$_data) {
            throw new notFoundException(__('Error 404 : Image file not found'));
        }

        $this->set(compact('_data'));

        $this->setView(
            'Show IMG#' . $imageId . ' | IMG Gallery',
            ['root', 'header-footer', 'default', 'showImage']
        );
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function add()
    {
        // on crée une Entity vide
        $images = $this->Images->newEmptyEntity();

        //Process post request
        if ($this->getRequest()->is('post')) {
            $_post = $this->getRequest()->getData();

            $image = $_post['image'];

            //Get extension
            $fileName = explode('.', $image->getClientFileName());
            $ext = '.' . array_pop($fileName);

            $date = date('YmdHis');

            //Path where you want to save the image
            $path = WWW_ROOT . '/' . _IMAGE_FOLDER .  strtolower(str_replace(' ', '_', $_post['title'])) . $date . $ext;

            //Upload the image
            $image->moveTo($path);

            //Check if it's correctly uploaded
            if (file_exists($path)) {
                $session = $this->getRequest()->getSession();

                $images->user_id = $session->read('User.id');
                $images->name = $_post['title'] ?? NULL;
                $images->path = _IMAGE_FOLDER .  strtolower(str_replace(' ', '_', $_post['title'])) . $date . $ext;
                $images->description = $_post['description'] ?? NULL;
                $images->width = getimagesize($path)[0] ?? NULL;
                $images->height = getimagesize($path)[1] ?? NULL;

                if (!preg_match('/^( +)?$/', $_post['author'])) :
                    $images->author = $_post['author'];
                else :
                    $images->author = 'Anonymous';
                endif;


                //Save images
                if ($this->Images->save($images)) {
                    $id = $images->id;
                }

                //Success
                $this->Flash->success('Your image has been create as id ' . $id . '.');
                $this->redirect('/show/' . $id);
            } else {

                //Failure
                $this->Flash->error('An error occurred while saving');
            }
        }

        $this->setView(
            'Add IMG | IMG Gallery',
            ['root', 'header-footer', 'default', 'addImage']
        );
    }

    /**
     * Delete method
     *
     * @param string|null $id Image id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     */
    public function delete()
    {
        $session = $this->getRequest()->getSession();

        $id = $this->getRequest()->getParam('id');

        $image = $this->Images->get($id);

        if ($image && ($image->user_id == $session->read('User.id') || $session->read('User.admin') == 1)) {

            //Security
            if ($this->referer() == '/show/' . $id) {

                //DELETE
                //-file
                unlink($image['path']);
                //-database
                $this->Images->delete($image);

                //Success
                $this->Flash->success('Your image has been delete');
                return $this->redirect($this->redirect('/home'));
            }
        } else {

            //Failure
            $this->Flash->error('An error occurred while deleting');
            return $this->redirect($this->referer());
        }
    }

    /**
     * Edit method
     *
     * @param int $id Image id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id)
    {
        $session = $this->getRequest()->getSession();
        $image = $this->Images->get($id, [
            'contain' => [],
        ]);
        if (!$image || $image->user_id != $session->read('User.id')) {
            return $this->redirect($this->referer());
        }
        if ($this->request->is('post')) {
            $image = $this->Images->patchEntity($image, $this->request->getData());
            if ($this->Images->save($image)) {
                $this->Flash->success(__('The image has been saved.'));

                return $this->redirect(['action' => 'show', 'pass' => ['$id']]);
            }
            $this->Flash->error(__('The image could not be saved. Please, try again.'));
        }
        $this->set(compact('image'));
        $this->setView(
            'edit IMG | IMG Gallery',
            ['root', 'header-footer', 'default', 'editImage']
        );
    }
}
