<?php

declare(strict_types=1);

namespace App\Controller;

/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 * @method \App\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        
        $this->Authentication->allowUnauthenticated(['add']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //Create an empty entity
        $comment = $this->Comments->newEmptyEntity();

        //Process post request
        if ($this->getRequest()->is('post')) {
            $_post = $this->getRequest()->getData();

            //Get image ID
            $image_id = explode('/', $this->referer());
            $image_id = array_pop($image_id);
            

            $comment->image_id = intval($image_id);
            
            if(!preg_match('/^( +)?$/',$_post['author'])):
                $comment->author = $_post['author'];
            else:
                $comment->author = 'Anonymous';
            endif;
            $comment->content = $_post['content'];
            
            //Save Comments
            if ($this->Comments->save($comment)) {
                $id = $comment->id;
            }

            $this->Flash->success('Your comment has been create as id ' . $id );
        }
        else{
            $this->Flash->error(
                'An error occurred while saving'
            );
        }
        
        $this->redirect($this->referer());
    }

    /**
     * Delete method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null|void.
     */
    public function delete()
    {
        
        $id = $this->getRequest()->getParam('pass')[0];

        $comment = $this->Comments->find()
        ->contain(['Images.Users'])
        ->where(['Comments.id' => $id])
        ->first();

        if($comment){
            
            $session = $this->getRequest()->getSession();

            if($comment->image->user->id == $session->read('User.id') || $session->read('User.admin') == 1){
                
                //DELETE
                //-database
                $this->Comments->delete($comment);

                //Success
                $this->Flash->success('Comment has been delete');
            }
        }else{

            //Failure
            $this->Flash->error('An error occurred while deleting');
        }
        $this->redirect($this->referer());
    }
}
