<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;

/**
 * Comment Entity
 *
 * @property int $id
 * @property int $image_id
 * @property string|null $author
 * @property string|null $content
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Image $image
 * @property \App\Model\Entity\CommentsContentTranslation $content_translation
 * @property \App\Model\Entity\I18n[] $_i18n
 */
class Comment extends Entity
{
    use TranslateTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'image_id' => true,
        'author' => true,
        'content' => true,
        'created' => true,
        'modified' => true,
        'image' => true,
        'content_translation' => true,
        '_i18n' => true,
    ];
}
